import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  email = ''
  password = ''
  
  constructor(
    private router: Router
  ) {}

  ngOnInit() {
  }
  ngOnDestroy() {
  }

  login() {
    if(this.email == '' || this.password == '') {
      alert('Username and Password is empty!')
      return false;
    }

    if(this.email != 'employee-test@gmail.com' && this.password != '123456') {
      alert('Username and Password is invalid!')
      return false;
    }

    this.createDummyData()
    this.router.navigateByUrl('/tables');
  }

  createDummyData() {
    let employeeArray = []
    const NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
    'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
    'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];

    for (let i = 0; i < 100; i++) {
      let birthDate = new Date('2000-12-17 00:00:00');
      birthDate.setDate(birthDate.getDate() + i);

      const firstName = NAMES[Math.round(Math.random() * (NAMES.length - 1))] 
      const lastName = NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.'+ i;

      let obj = {
        username: `${firstName}-test${i}`,
        firstName: firstName,
        lastName: lastName,
        email: `${firstName}-${lastName}${i}@gmail.com`,
        birthDate: birthDate,
        basicSallary: `5000000`,
        status: `Active`,
        group: `Group 1`,
        description: new Date(),
      }
      employeeArray.push(obj)
    }

    window.localStorage.setItem('employees', JSON.stringify(employeeArray))
  }

}
