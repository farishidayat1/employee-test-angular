import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.scss']
})
export class TablesComponent implements OnInit {
  employees: any = []
  page = 1;           
  collectionSize = 100
  pageSize = 10;   
  public focus;
  searchValue = ''
  pageType = 'list'

  username = new FormControl('');
  firstName = new FormControl('');
  lastName = new FormControl('');
  email = new FormControl('');
  birthDate = new FormControl('');
  basicSallary = new FormControl('');
  status = new FormControl('');
  group = new FormControl('');

  employeeDetail: any = {}
  isSubmit = false;

  constructor() { 
  }

  ngOnInit() {
    this.refreshEmployees()
  }

  refreshEmployees(){
    this.employees = JSON.parse(window.localStorage.getItem('employees')).map((country, i) => ({id: i + 1, ...country})).slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  filterBy() {
    if (this.searchValue) {
      this.employees = this.employees.filter(item =>
        item.firstName.toLowerCase().includes(this.searchValue.toLowerCase())
      );
    }
  }

  changePage(page, employee = null) {
    if(page == 'detail') {
      this.employeeDetail = employee
      let currency = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
      }).format(this.employeeDetail.basicSallary);
      this.employeeDetail.basicSallary = currency
    }
    if(page == 'list') {
      this.refreshEmployees()
    }
    this.pageType = page
  }

  submit() {
    this.isSubmit = true;
    if(this.username.value == '' || this.firstName.value == '' || this.lastName.value == '' || this.email.value == '' || this.birthDate.value == '' || this.status.value == '' || this.group.value == '') {
      return false;
    }

    let employeeArray = JSON.parse(window.localStorage.getItem('employees'))
    let obj = {
      username: this.username.value,
      firstName: this.firstName.value,
      lastName: this.lastName.value,
      email: this.email.value,
      birthDate: this.birthDate.value,
      basicSallary: this.basicSallary.value,
      status: this.status.value,
      group: this.group.value,
      description: new Date(),
    }
    employeeArray.push(obj)

    window.localStorage.removeItem('employees')
    window.localStorage.setItem('employees', JSON.stringify(employeeArray))
    this.changePage('list')
  }

}
